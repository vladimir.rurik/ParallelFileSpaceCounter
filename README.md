# Parallel File Space Counter

This project includes a C# application that reads all files from a specified folder in parallel and calculates the total number of spaces in these files. The execution time for the operation is measured using the `Stopwatch` class.

## Features

- Reads all text files from a specified directory.
- Counts the total number of space characters in these files in parallel.
- Measures the execution time of reading and processing files.

## Usage

To use this application, specify the path to the folder containing your text files. The application will then read each file, count the number of spaces, and output the total count along with the time taken to perform this operation.
