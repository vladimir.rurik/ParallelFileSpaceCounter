﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Linq;

class Program
{
    static async Task Main(string[] args)
    {
        // Set the folder path for reading files.
        string folderPath = "../../../TestData"; // Replace with your actual folder path

        // Check if the directory exists to avoid errors from an invalid path.
        if (!Directory.Exists(folderPath))
        {
            Console.WriteLine("Directory does not exist.");
            return;
        }

        var stopwatch = new Stopwatch();
        stopwatch.Start();

        // Count the total number of spaces in all files within the folder.
        int totalSpaces = await CountSpacesInFiles(folderPath);

        stopwatch.Stop();

        // Output the results to the console.
        Console.WriteLine($"Total number of spaces: {totalSpaces}");
        Console.WriteLine($"Time taken: {stopwatch.ElapsedMilliseconds} ms");
    }

    static async Task<int> CountSpacesInFiles(string folderPath)
    {
        // Create a task for each file to count spaces and process them in parallel.
        var fileTasks = Directory.GetFiles(folderPath)
                                 .Select(filePath => CountSpacesInFile(filePath));

        // Wait for all tasks to complete and sum up their results.
        var results = await Task.WhenAll(fileTasks);

        return results.Sum();
    }

    static async Task<int> CountSpacesInFile(string filePath)
    {
        try
        {
            // Read the file content asynchronously and count the number of space characters.
            string content = await File.ReadAllTextAsync(filePath);
            return content.Count(c => c == ' ');
        }
        catch (IOException ex)
        {
            // Handle potential file reading errors.
            Console.WriteLine($"Error reading file {filePath}: {ex.Message}");
            return 0;
        }
    }
}
